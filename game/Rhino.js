	function Rhino(texture, X,Y){

	this.texture;
	this.Position;
	this.STEP = 5;
	this.Width = 0;
	this.Height = 0;
	this.Hitbox;
	this.Health = 100;
	this.invisible;

	this.create = function(texture, X, Y){
		this.Position = new Vector2(X, Y);
		this.Hitbox = new Hitbox(this.Position.getX(), this.Position.getY(), 192, 96);
		this.texture = texture;
		this.Width = new Display().getWidth();
		this.Height = new Display().getHeight();
	}

 	this.render = function(){
 		if (this.Health <= 10){
 			this.destroy();
 		}
 		if (!(typeof this.Hitbox === "undefined")) {
	 		this.Hitbox = new Hitbox(this.Position.getX(), this.Position.getY(), 192, 96);
	 	}
 		this.Position.setX(this.Position.getX() - this.STEP);
  		if (this.Position.getX() < (0 - 192) ||
 			this.Position.getX() > (this.Width + 192)){
 			this.destroy();
 		}
		if (!this.invisible){
			new GL().renderTexture(this.texture, this.Position.getX(), this.Position.getY());
		}
 	}

 	this.destroy = function(){
 		this.invisible = true;
 		this.Hitbox = undefined;
 	}

	this.create(texture, X,Y);

}