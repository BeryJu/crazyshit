importPackage(org.BeryJu.RhinoGame);
importPackage(org.BeryJu.RhinoGame.RenderSystem);
@load<javascript_game>
@debug<on>
var Instance = Rhino_Instance;
var Arguments = Rhino_Arguments;
function main(){
	inst = new RhinoGame(Instance, Arguments);
	try{
		inst.run();
	}catch (err){
		stackTrace(err);
	}
}

function RhinoGame(Instance, Arguments){

	public _Instance = Instance;
	public _Arguments = Arguments;
	public _RessourceLoader = undefined;
	public _Display = undefined;
	public _Camera = undefined;
	public _GL = undefined;
	public _AL = undefined;
	public _Input = undefined;
	public _ResolutionW = 800;
	public _ResolutionH = 600;
	//Main font
	public _Font;
	public _Player;
	public _RhinoTexture;
	public _Rhino = [];
	public _ShootSound;
	public _HitSound;

	public run(){
		this.initGL();
		this.initInput();
		this.intRessources();
		this.initAL();
		while (!this._Display.isCloseRequested()) {
			this.render();
		}
	}

	public initGL(){
		//The actual canvas
		this._Display = new Display();
		//Width, Height
		this._Display.setDisplayMode(this._ResolutionW, this._ResolutionH);
		this._Display.setTitle("Crazy Shit");
		this._Display.create();
		//Open_GL class
		this._GL = new GL();
		this._GL.initOpenGL(this._ResolutionW, this._ResolutionH);
		Utils.log("OpenGL successfully initialized", this);
	}

	public initAL(){
		this._AL = new AL();
		this._ShootSound = this._RessourceLoader.loadSound("plop");
		this._HitSound =  this._RessourceLoader.loadSound("punch");
	}

	public initInput(){
		//_Input handling stuff
		this._Input = new Input();
		this._Input.bind(this._Input.KEY_ESCAPE, this._Input.KEY_MODE_TAP, function(parent){
			new Display().requestClose();
		});
		this._Input.bind(this._Input.KEY_A, this._Input.KEY_MODE_PRESS, function(parent){
			parent._Player._IsLeft = true;
			parent._Player._Position.setX(parent._Player._Position.getX() - 5);
		});
		this._Input.bind(this._Input.KEY_D, this._Input.KEY_MODE_PRESS, function(parent){
			parent._Player._IsLeft = false;
			parent._Player._Position.setX(parent._Player._Position.getX() + 5);
		});
		this._Input.bind(this._Input.KEY_SPACE, this._Input.KEY_MODE_TAP, function(parent){
			parent._Player.jump();
		});
		this._Input.bind(this._Input.KEY_E, this._Input.KEY_MODE_TAP, function(parent){
			parent._Rhino.push(new Rhino(parent._RhinoTexture,
					parent._ResolutionW - 192, parent._ResolutionH - 96,
					 parent._ResolutionW, parent._HitSound));
		});
		this._Input.bind(this._Input.KEY_W, this._Input.KEY_MODE_TAP, function(parent){
			parent._Player.shoot();
			parent._ShootSound.sound(1, 1, false);
		});
	}

	public intRessources(){
		this._RessourceLoader = new RessourceLoader();
		this._Font = this._RessourceLoader.loadFont("ubuntu", 28);
		//Player
		this._Player = new Player("BeryJu", this._Font);
		this._RhinoTexture = this._RessourceLoader.loadTexture("Rhino_left");
		this._Rhino.push(new Rhino(this._RhinoTexture,
				this._ResolutionW - 192, this._ResolutionH - 96, this._ResolutionW));
		this._RessourceLoader.loadEntity();
		Utils.log("Running on "+Utils.getOS(), this);
	}

	public render(){
		this._GL.clearCanvas();
		var fps = this._GL.FPS.getFPS();
		//X, Y, Text, Color
		this._Font.drawString(0,0, "RhinoGame Engine build"+this._Instance.Build);
		this._Font.drawString(0,30,fps.toString()+"FPS, "
			+ this._Instance.getMemoryPercentage().toString()
			+"% Memory used",  Color.white);
		this._Font.drawString(0,60,this._Player._Name+"'s Score: "+this._Player._Score);
		this._Font.drawString(0,90,this._Player._Name+"'s health: "+this._Player._Health);
		this._Player.render();
		
		this._Input.check(this);
		foreach(this._Rhino : rhino){
			try{
				rhino.render();
			}catch(err){
				stackTrace(err);
			}
		}
		foreach(this._Player._Projectiles : projectile){
			try{
				projectile.render();
			}catch(err){
				stackTrace(err);
			}
		}
		var p = this._Player;
		foreach(this._Rhino : rhino){
			var r = rhino._Hitbox
			try{
				if (r.intersects(p.getHitbox())){
					p._Health -= 10;
					rhino._Hit.sound(1, 1, false);
					rhino.destroy();
				}
			}catch (err){
				// stackTrace(err);
			}
			foreach(p._Projectiles : projectile){
				try{
					if (projectile.Hitbox.intersects(r)){
						rhino._Health -= 10;
						projectile.destroy();
						if (rhino._Health <= 10){
				 			rhino.destroy();
				 			rhino = undefined;
				 			this._Score += 10;
				 		}
					}
				}catch (err){
					// stackTrace(err);
				}
			}
		}
		this._GL.render();
		this._AL.put();
	}

}