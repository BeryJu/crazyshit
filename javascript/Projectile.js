function Projectile(texture,from, to){

	this.texture;	
	this.position;
	this.Destination;
	this.Speed = new Vector2(5,5);
	this.Width = 0;
	this.Hitbox;
	this.invisible;

	this.new = function(texture,from, to){
		this.position = from;
		this.Destination = to;
 		if (!(typeof this.Hitbox === "undefined")) {
	 		this.Hitbox = new Hitbox(this.position.getX(), this.position.getY(), 48, 24);
	 	}
		this.texture = texture;
		this.Width = new Display().getWidth();
	}

 	this.destroy = function(){
 		this.invisible = true;
 		this.texture = undefined
 		this.Hitbox = undefined;
 	}

 	this.render = function(){
 		try{
	 		if (this.position.equals(this.Destination)){
	 			this.destroy();
	 		}
			this.position = this.position.VectorInterpolate(this.position, this.Destination, this.Speed);
			this.Hitbox = new Hitbox(this.position.getX(), this.position.getY(), 48, 24);
	 		if (!this.invisible){
		 		new GL().renderTexture(this.texture, this.position.getX(), this.position.getY());
	 		}
 		}catch(err){
 			Utils.log(err, this);
 		}
 	}

	this.new(texture,from, to);

}