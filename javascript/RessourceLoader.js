// importPackage();
class RessourceLoader{

	public loadEntity(){
		var e = Utils.deserialize(this.test());
		Utils.log(e.toString(), this);
	}

	public test(){
		var e = new Entity();
		e._ID = "derp";
		e._TextureKey = "herpderp";
		return Utils.serialize(e);
	}

	this.loadTexture = function(key){
		if (new File("assets/textures/"+key+".tga").exists() == false) {
			Utils.log("Texture \'"+key+"\' not found", this);
		} else {
			Utils.log("Loaded Texture \'"+key+"\'", this);
			return TextureLoader.getTexture("TGA",
					ResourceLoader.getResourceAsStream("assets/textures/"+key+".tga"));
		}
	}

	this.loadSound = function(filename){
		Utils.log("Loaded Sound \'"+filename+"\'", this);
		var a = new Audio();
		a._Data = AudioLoader.getAudio("OGG", ResourceLoader.
			getResourceAsStream("assets/sound/"+filename+".ogg"));
		return a;
	}

	this.loadFont = function(name, size){
		if (new File("assets/font/"+name+".ttf").exists()) {
			try{
				var inputStream = ResourceLoader.
					getResourceAsStream("assets/font/"+name+".ttf");
				var awtFont2 = java.awt.Font.
					createFont(java.awt.Font.TRUETYPE_FONT, inputStream);
				awtFont2 = awtFont2.deriveFont(size); // set font size
				return TrueTypeFont(awtFont2, true);
			}catch (e){
				Utils.log(e, this);
			}
		} else {
			Utils.log("Font "+name+" could not be loaded", null);
		}
	}

}