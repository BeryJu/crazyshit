
importPackage(org.lwjgl.input);
importPackage(org.lwjgl.opengl);
importPackage(org.lwjgl.util.glu);

// importClass(java.lang.Math);
importClass(org.lwjgl.opengl.ARBDepthClamp);
// importClass(org.lwjgl.opengl.GL11);

/**
 * A camera set in 3D perspective. The camera uses Euler angles internally, so beware of a gimbal lock.
 *
 * @author Oskar Veerhoek
 * @ported by BeryJu
 */
function EulerCamera(builder) {

	this.x = 0;
	this.y = 0;
	this.z = 0;
	this.pitch = 0;
	this.yaw = 0;
	this.roll = 0;
	this.fov = 90;
	this.aspectRatio = 1;
	this.zNear = 0;
	this.zFar = 0;

	this.create = function(builder) {
		this.x = builder.x;
		this.y = builder.y;
		this.z = builder.z;
		this.pitch = builder.pitch;
		this.yaw = builder.yaw;
		this.roll = builder.roll;
		this.aspectRatio = builder.aspectRatio;
		this.zNear = builder.zNear;
		this.zFar = builder.zFar;
		this.fov = builder.fov;
	}

	this.processMouse = function(mouseSpeed, maxLookUp, maxLookDown) {
		var mouseDX = Mouse.getDX() * mouseSpeed * 0.16;
		var mouseDY = Mouse.getDY() * mouseSpeed * 0.16;
		if (this.yaw + mouseDX >= 360) {
			this.yaw = this.yaw + mouseDX - 360;
		} else if (this.yaw + mouseDX < 0) {
			this.yaw = 360 - this.yaw + mouseDX;
		} else {
			this.yaw += mouseDX;
		}
		if (this.pitch - mouseDY >= maxLookDown
				&& this.pitch - mouseDY <= maxLookUp) {
			this.pitch += -mouseDY;
		} else if (this.pitch - mouseDY < maxLookDown) {
			this.pitch = maxLookDown;
		} else if (this.pitch - mouseDY > maxLookUp) {
			this.pitch = maxLookUp;
		}
	}

	 this.processKeyboard = function(delta, speed) {
	 	var i = new Input();
	 	var ke = i.Keyboard();
		var keyUp = ke.isKeyDown(i.KEY_UP) || 
			ke.isKeyDown(i.KEY_W);
		var keyDown = ke.isKeyDown(i.KEY_DOWN) || 
			ke.isKeyDown(i.KEY_S);
		var keyLeft = ke.isKeyDown(i.KEY_LEFT) || 
			ke.isKeyDown(i.KEY_A);
		var keyRight = ke.isKeyDown(i.KEY_RIGHT) || 
			ke.isKeyDown(i.KEY_D);
		var flyUp = ke.isKeyDown(i.KEY_SPACE);
		var flyDown = ke.isKeyDown(i.KEY_LSHIFT);

		if (keyUp && keyRight && !keyLeft && !keyDown) {
			this.moveFromLook(speed * delta * 0.003, 0, -speed * delta * 0.003);
		}
		if (keyUp && keyLeft && !keyRight && !keyDown) {
			this.moveFromLook(-speed * delta * 0.003, 0, -speed * delta * 0.003);
		}
		if (keyUp && !keyLeft && !keyRight && !keyDown) {
			this.moveFromLook(0, 0, -speed * delta * 0.003);
		}
		if (keyDown && keyLeft && !keyRight && !keyUp) {
			this.moveFromLook(-speed * delta * 0.003, 0, speed * delta * 0.003);
		}
		if (keyDown && keyRight && !keyLeft && !keyUp) {
			this.moveFromLook(speed * delta * 0.003, 0, speed * delta * 0.003);
		}
		if (keyDown && !keyUp && !keyLeft && !keyRight) {
			this.moveFromLook(0, 0, speed * delta * 0.003);
		}
		if (keyLeft && !keyRight && !keyUp && !keyDown) {
			this.moveFromLook(-speed * delta * 0.003, 0, 0);
		}
		if (keyRight && !keyLeft && !keyUp && !keyDown) {
			this.moveFromLook(speed * delta * 0.003, 0, 0);
		}
		if (flyUp && !flyDown) {
			this.y += speed * delta * 0.003;
		}
		if (flyDown && !flyUp) {
			this.y -= speed * delta * 0.003;
		}
	}

	this.toRadians = function(deg){
		return deg  * (Math.PI / 180);
	}

	this.moveFromLook = function(dx, dy, dz) {
		this.x -= dx * parseFloat(Math.sin(this.toRadians(this.yaw - 90)) +
			dz * Math.sin(this.toRadians(this.yaw)));
		this.y += dy * parseFloat(Math.sin(this.toRadians(this.pitch - 90)) +
			dz * Math.sin(this.toRadians(this.pitch)));
		this.z += dx * parseFloat(Math.cos(this.toRadians(this.yaw - 90)) +
			dz * Math.cos(this.toRadians(this.yaw)));
		//var hypotenuseX = dx;
		//var adjacentX = hypotenuseX * (var) Math.cos(Math.toRadians(yaw - 90));
		//var oppositeX = (var) Math.sin(Math.toRadians(yaw - 90)) * hypotenuseX;
		//this.z += adjacentX;
		//this.x -= oppositeX;
		//
		//this.y += dy;
		//
		//var hypotenuseZ = dz;
		//var adjacentZ = hypotenuseZ * (var) Math.cos(Math.toRadians(yaw));
		//var oppositeZ = (var) Math.sin(Math.toRadians(yaw)) * hypotenuseZ;
		//this.z += adjacentZ;
		//this.x -= oppositeZ;
	}

	this.setPosition = function(x, y, z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	this.applyOrthographicMatrix = function() {
		GL11.glPushAttrib(GL11.GL_TRANSFORM_BIT);
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(-this.aspectRatio, this.aspectRatio, -1, 1, 0, this.zFar);
		GL11.glPopAttrib();
	}

	this.applyOptimalStates = function() {
		if (GLContext.getCapabilities().GL_ARB_depth_clamp) {
			GL11.glEnable(34383);
		}
	}

	this.applyPerspectiveMatrix = function() {
		GL11.glPushAttrib(GL11.GL_TRANSFORM_BIT);
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GLU.gluPerspective(this.fov, this.aspectRatio, this.zNear, this.zFar);
		GL11.glPopAttrib();
	}

	this.applyTranslations = function() {
		GL11.glPushAttrib(GL11.GL_TRANSFORM_BIT);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glRotatef(this.pitch, 1, 0, 0);
		GL11.glRotatef(this.yaw, 0, 1, 0);
		GL11.glRotatef(this.roll, 0, 0, 1);
		GL11.glTranslatef(-this.x, -this.y, -this.z);
		GL11.glPopAttrib();
	}

	this.setRotation = function(thispitch, yaw, roll) {
		this.thispitch = thispitch;
		this.yaw = yaw;
		this.roll = roll;
	}

	this.setAspectRatio = function(aspectRatio) {
		if (aspectRatio <= 0) {
			throw new IllegalArgumentException("aspectRatio " + aspectRatio + " is 0 or less");
		}
		this.aspectRatio = aspectRatio;
	}

	this.toString = function() {
		return "EulerCamera [x=" + x + ", y=" + y + ", z=" + z + ", thispitch=" + thispitch
				+ ", yaw=" + yaw + ", roll=" + roll + ", fov=" + fov
				+ ", aspectRatio=" + aspectRatio + ", zNear=" + zNear
				+ ", zFar=" + zFar + "]";
	}

	this.create(builder);

}
function Builder() {
	this.aspectRatio = 1;
	this.x = 0;
	this.y = 0;
	this.z = 0;
	this.pitch = 0;
	this.yaw = 0;
	this.roll = 0;
	this.zNear = 0.3;
	this.zFar = 100;
	this.fov = 90;

	this.setAspectRatio = function(aspectRatio) {
		this.aspectRatio = aspectRatio;
		return this;
	}

	this.setNearClippingPane = function(nearClippingPane) {
		this.zNear = nearClippingPane;
		return this;
	}

	this.setFarClippingPane = function(farClippingPane) {
		this.zFar = farClippingPane;
		return this;
	}

	this.setFieldOfView = function(fov) {
		this.fov = fov;
		return this;
	}

	this.setPosition = function( x,  y,  z) {
		this.x = x;
		this.y = y;
		this.z = z;
		return this;
	}

	this.setRotation = function( pitch,  yaw,  roll) {
		this.pitch = pitch;
		this.yaw = yaw;
		this.roll = roll;
		return this;
	}

	this.build = function() {
		return new EulerCamera(this);
	}
}