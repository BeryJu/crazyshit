function each(array, callback, parent){
	if (isFunction(callback)){
		for (var i = 0; i < array.length; i++) {
			callback(array[i], parent);
		}
	}else{
		Utils.log("callback is not function, must be function", this);
	}
}

function isFunction(functionToCheck) {
	var getType = {};
	return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
}