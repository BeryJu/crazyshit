function Rhino(texture, X,Y, Width){

	this.texture;
	this.Position;
	this.STEP = 5;
	this.Width = 0;
	this.Hitbox;
	this.Health = 100;
	this.invisible;

	this.new = function(texture, X, Y, Width){
		this.Position = new Vector2(X, Y);
		this.Hitbox = new Hitbox(this.Position.getX(), this.Position.getY(), 192, 96);
		this.texture = texture;
		this.Width = Width;
	}

 	this.render = function(){
 		if (this.Health <= 10){
 			this.destroy();
 		}
 		if (!(typeof this.Hitbox === "undefined")) {
	 		this.Hitbox = new Hitbox(this.Position.getX(), this.Position.getY(), 192, 96);
	 	}
 		this.Position.getX() -= this.STEP;
  		if (this.Position.getX() < (0 - 192) ||
 			this.Position.getX() > (1280 + 192)){
 			this.destroy();
 		}
		if (!this.invisible){
			new GL().renderTexture(this.texture, this.Position.getX(), this.Position.getY());
		}
 	}

 	this.destroy = function(){
 		this.invisible = true;
 		this.Hitbox = undefined;
 	}

	this.new(texture, X,Y, Width);

}