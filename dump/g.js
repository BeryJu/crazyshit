importPackage(org.BeryJu.RhinoGame);
importPackage(org.BeryJu.RhinoGame.RenderSystem);
//Projectile.js
function Projectile(texture,from, to){

	this.texture;	
	this.position;
	this.Direction;
	this.Speed = new Vector2(5,5);
	this.Width = 0;
	this.Hitbox;
	this.invisible;

	this.create = function(texture,from, to){
		this.position = from;
		this.Direction = VectorNormalized(VectorSub(to, this.position));
 		if (!(typeof this.Hitbox === "undefined")) {
	 		this.Hitbox = new Hitbox(this.position.getX(), this.position.getY(), 48, 24);
	 	}
		this.texture = texture;
		this.Width = new Display().getWidth();
	}

 	this.destroy = function(){
 		this.invisible = true;
 		this.texture = undefined
 		this.Hitbox = undefined;
 	}

 	this.isOffScreen = function(){
 		var width = new Display().getWidth();
 		var height = new Display().getHeight();
 		if (this.position.getX() < 0-48 ||
 			this.position.getX() > width+48){
 			return true;
 		}
 		if (this.position.getY() < 0-48 ||
 			this.position.getY() > height+48){
			return true;
		}
		return false;
 	}

 	this.render = function(){
 		try{
	 		if (this.isOffScreen() == true){
	 			this.destroy();
	 		}
			this.position = VectorAdd(this.position, VectorMul(this.Direction, this.Speed));
	 		//new Display().setTitle(this.position.toString());
			this.Hitbox = new Hitbox(this.position.getX(), this.position.getY(), 48, 24);
	 		if (!this.invisible){
		 		new GL().renderTexture(this.texture, this.position.getX(), this.position.getY());
	 		}
 		}catch(err){
 			Utils.log(err, this);
 		}
 	}

	this.create(texture,from, to);

}

//Player.js
function Player(name, font){

	this._IsLeft;
	this._Name;
	this._TextureLeft;
	this._TextureRight;
	this._TextureProjectile;
	this._Projectiles = [];
	this._Position;
	this._Health = 100;
	this._Hitbox;
	this._Font;
	this._Score = 0;
	// this.JumpState { Up, Down, None }
	// this.isJump;
	// this.jumpState = "None";
	// this.beginJumpY;
	// this.jumpSpeed = 1.8;
	// this.jumpHeight = 80.0;

	this.create = function(name, font){
		this._Name = name;
		this._Font = font;
		this._Position = new Vector2(0, (new Display().getHeight() - (50 + font.getHeight())));
		this._Hitbox = new Hitbox(this._Position.getX()+10, this._Position.getY(), 48, 27);
		this._TextureLeft = new RessourceLoader().loadTexture("Monkey_left");
		this._TextureRight = new RessourceLoader().loadTexture("Monkey_right");
		this._TextureProjectile = new RessourceLoader().loadTexture("Projectile");
	}

	this.getHitbox = function(){
		return this._Hitbox;
	}

	// this.jump = function(){
	// 	this.isJump = true;
	// 	this.jumpState = "Up";
	// 	this.beginJumpY = this._Position.getY();
	// }
	//
	// this.checkJump = function(){
	// 	if (this.isJump){
	// 		if (this.jumpState == "Up"){
	// 			this._Position.setY(this._Position.getY() + this.jumpHeight * this.jumpSpeed);
	// 			if (this._Position.getY() >= this.beginJumpY){
	// 				this.isJump = false;
	// 				this.jumpState = "Down";
	// 			}
	// 		}else{
	// 			this._Position.setY(this._Position.getY() - this.beginJumpY * this.jumpSpeed);
	// 			if (this._Position.getY() <= this.beginJumpY){
	// 				this.isJump = false;
	// 				this.jumpState = "None";
	// 				this._Position.setY(0);
	// 			}
	// 		}
	// 	}
	// }

	this.render = function(){
		this._Hitbox = new Hitbox(this._Position.getX()+10, this._Position.getY(), 48, 27);
		this._Font.drawString(this._Position.getX(), this._Position.getY(), this._Name);
		if (this._IsLeft){
			new GL().renderTexture(this._TextureLeft, this._Position.getX(),
				 this._Position.getY() + this._Font.getHeight(), 1);
		}else{
			new GL().renderTexture(this._TextureRight, this._Position.getX(),
				 this._Position.getY() + this._Font.getHeight(), 1);
		}
	}

	this.shoot = function(){
		this._Projectiles.push(new Projectile(this._TextureProjectile,
			this._Position, new Input().getMousePos())); 
	}

	this.create(name, font);

}

//Rhino.js
function Rhino(texture, X,Y, sound){

	this.STEP = 5;
	this._Texture;
	this._Position;
	this._Width = 0;
	this._Height = 0;
	this._Hitbox;
	this._Health = 100;
	this._Invisible;
	this._Hit;

	this.create = function(texture, X, Y, sound){
		this._Hit = sound;
		this._Position = new Vector2(X, Y);
		this._Hitbox = new Hitbox(this._Position.getX(), this._Position.getY(), 192, 96);
		this._Texture = texture;
		this._Width = new Display().getWidth();
		this._Height = new Display().getHeight();
	}

 	this.render = function(){
 		if (this._Health <= 10){
 			this.destroy();
		}
 		if (!(typeof this._Hitbox === "undefined")) {
	 		this._Hitbox = new Hitbox(this._Position.getX(), this._Position.getY(), 192, 96);
	 	}
 		this._Position.setX(this._Position.getX() - this.STEP);
  		if (this._Position.getX() < (0 - 192) ||
 			this._Position.getX() > (this.Width + 192)){
 			this.destroy();
 		}
		if (!this._Invisible){
			new GL().renderTexture(this._Texture, this._Position.getX(), this._Position.getY());
		}
 	}

 	this.destroy = function(){
 		this._Invisible = true;
 		this._Hitbox = undefined;
 	}

	this.create(texture, X,Y, sound);

}



var Instance = Rhino_Instance;
var Arguments = Rhino_Arguments;
function main(){
	inst = new RhinoGame(Instance, Arguments);
	try{
		inst.run();
	}catch (err){
		stackTrace(err);
	}
}

function RhinoGame(Instance, Arguments){

this._Instance = Instance;
this._Arguments = Arguments;
this._RessourceLoader = undefined;
this._Display = undefined;
this._Camera = undefined;
this._GL = undefined;
this._AL = undefined;
this._Input = undefined;
this._ResolutionW = 800;
this._ResolutionH = 600;
	//Main font
this._Font;
this._Player;
this._RhinoTexture;
this._Rhino = [];
this._ShootSound;
this._HitSound;

this.run = function(){
		this.initGL();
		this.initInput();
		this.intRessources();
		this.initAL();
		while (!this._Display.isCloseRequested()) {
			this.render();
		}
	}

this.initGL = function(){
		//The actual canvas
		this._Display = new Display();
		//Width, Height
		this._Display.setDisplayMode(this._ResolutionW, this._ResolutionH);
		this._Display.setTitle("Crazy Shit");
		this._Display.create();
		//Open_GL class
		this._GL = new GL();
		this._GL.initOpenGL(this._ResolutionW, this._ResolutionH);
		Utils.log("OpenGL successfully initialized", this);
	}

this.initAL = function(){
		this._AL = new AL();
		this._ShootSound = this._RessourceLoader.loadSound("plop");
		this._HitSound =  this._RessourceLoader.loadSound("punch");
	}

this.initInput = function(){
		//_Input handling stuff
		this._Input = new Input();
		this._Input.bind(this._Input.KEY_ESCAPE, this._Input.KEY_MODE_TAP, function(parent){
			new Display().requestClose();
		});
		this._Input.bind(this._Input.KEY_A, this._Input.KEY_MODE_PRESS, function(parent){
			parent._Player._IsLeft = true;
			parent._Player._Position.setX(parent._Player._Position.getX() - 5);
		});
		this._Input.bind(this._Input.KEY_D, this._Input.KEY_MODE_PRESS, function(parent){
			parent._Player._IsLeft = false;
			parent._Player._Position.setX(parent._Player._Position.getX() + 5);
		});
		this._Input.bind(this._Input.KEY_SPACE, this._Input.KEY_MODE_TAP, function(parent){
			parent._Player.jump();
		});
		this._Input.bind(this._Input.KEY_E, this._Input.KEY_MODE_TAP, function(parent){
			parent._Rhino.push(new Rhino(parent._RhinoTexture,
					parent._ResolutionW - 192, parent._ResolutionH - 96,
					 parent._ResolutionW, parent._HitSound));
		});
		this._Input.bind(this._Input.KEY_W, this._Input.KEY_MODE_TAP, function(parent){
			parent._Player.shoot();
			parent._ShootSound.sound(1, 1, false);
		});
	}

this.intRessources = function(){
		this._RessourceLoader = new RessourceLoader();
		this._Font = this._RessourceLoader.loadFont("ubuntu", 28);
		//Player
		this._Player = new Player("BeryJu", this._Font);
		this._RhinoTexture = this._RessourceLoader.loadTexture("Rhino_left");
		this._Rhino.push(new Rhino(this._RhinoTexture,
				this._ResolutionW - 192, this._ResolutionH - 96, this._ResolutionW));
		this._RessourceLoader.loadEntity();
	}

this.render = function(){
		this._GL.clearCanvas();
		var fps = this._GL.FPS.getFPS();
		//X, Y, Text, Color
		this._Font.drawString(0,0, "RhinoGame Engine build"+this._Instance.Build);
		this._Font.drawString(0,30,fps.toString()+"FPS, "
			+ this._Instance.getMemoryPercentage().toString()
			+"% Memory used",  Color.white);
		this._Font.drawString(0,60,this._Player._Name+"'s Score: "+this._Player._Score);
		this._Font.drawString(0,90,this._Player._Name+"'s health: "+this._Player._Health);
		this._Player.render();
		
		this._Input.check(this);
for (var i = 0; i < this._Rhino .length; i++) {
var  rhino = this._Rhino [i];
			try{
				rhino.render();
			}catch(err){
				stackTrace(err);
			}
		}
for (var i = 0; i < this._Player._Projectiles .length; i++) {
var  projectile = this._Player._Projectiles [i];
			try{
				projectile.render();
			}catch(err){
				stackTrace(err);
			}
		}
		var p = this._Player;
for (var i = 0; i < this._Rhino .length; i++) {
var  rhino = this._Rhino [i];
			var r = rhino._Hitbox
			try{
				if (r.intersects(p.getHitbox())){
					p._Health -= 10;
					rhino._Hit.sound(1, 1, false);
					rhino.destroy();
				}
			}catch (err){
				// stackTrace(err);
			}
			// this.projectiles(rhino);
		}
		this._GL.render();
		this._AL.put();
	}

	// public projectiles(rhino){
	// 	foreach(this._Projectiles : projectile){
	// 		try{
	// 			if (projectile.Hitbox.intersects(r)){
	// 				rhino._Health -= 10;
	// 				projectile.destroy();
	// 				if (rhino._Health <= 10){
	// 		 			rhino.destroy();
	// 		 			rhino = undefined;
	// 		 			this._Score += 10;
	// 		 		}
	// 			}
	// 		}catch (err){
	// 			// stackTrace(err);
	// 		}
	// 	}
	// }


}
