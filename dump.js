//GL.js
importPackage(org.lwjgl);
importPackage(org.lwjgl.opengl);
importPackage(org.BeryJu.RhinoGame);
importPackage(java.io);
importPackage(java.awt.image);
importPackage(java.nio);
importPackage(javax.imageio);
importClass(java.awt.Font);
importPackage(org.newdawn.slick);
importClass(org.newdawn.slick.util.ResourceLoader);
importPackage(org.newdawn.slick.opengl);
function GL(){

	this.takeScreenshot = function(){
		GL11.glReadBuffer(GL11.GL_FRONT);
		var width = new Display().getWidth();
		var height= new Display().getHeight();
		var bpp = 4; // Assuming a 32-bit display with a byte each for red, green, blue, and alpha.
		var buffer = BufferUtils.createByteBuffer(width * height * bpp);
		GL11.glReadPixels(0, 0, width, height, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer );
		var file = new File(dir+"/"+System.currentTimeMillis()+".png");
		var format = "PNG";
		var image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

		for(var x = 0; x < width; x++){
			for(var y = 0; y < height; y++){
				var i = (x + (width * y)) * bpp;
				var r = buffer.get(i) & 0xFF;
				var g = buffer.get(i + 1) & 0xFF;
				var b = buffer.get(i + 2) & 0xFF;
				image.setRGB(x, height - (y + 1), (0xFF << 24) | (r << 16) | (g << 8) | b);
			}
		}
		try {
			ImageIO.write(image, format, file);
		} catch (error) {
			Utils.log("Error during File I/O: "+error, this);
		}
		Utils.log("Saved Screenshot "+file.getName(), this);
	}

	this.loadTexture = function(key){
		if (new File("assets/"+key+".tga").exists() == false) {
			Utils.log("Texture \'"+key+"\' not found", this);
		} else {
			Utils.log("Loaded Texture \'"+key+"\'", this);
			return TextureLoader.getTexture("TGA",
					ResourceLoader.getResourceAsStream("assets/"+key+".tga"));
		}
	}

	this.initOpenGL = function(w, h){
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, w, h, 0, 0, -1);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
	}

	this.render = function(){
		org.lwjgl.opengl.Display.sync(60);
		org.lwjgl.opengl.Display.update();
		this.FPS.update();
		this.FPS.frameTime();
	}

	this.clearCanvas = function(){
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT); 
	}

	this.renderTexture = function(texture, x, y, r){
		x = parseInt(x);
		y = parseInt(y);
		r = parseInt(r);
		if (typeof(x) === "number" && typeof(y) === "number"){
			try{
				texture.bind();
				GL11.glColor3f(1, 1, 1);
				GL11.glRotatef(r, 0, 0, 1);
				GL11.glBegin(GL11.GL_QUADS);
				GL11.glTexCoord2f(0, 0);
				GL11.glVertex2f(x, y);
				GL11.glTexCoord2f(texture.getWidth(), 0);
				GL11.glVertex2f(x + texture.getImageWidth(), y);
				GL11.glTexCoord2f(texture.getWidth(), texture.getHeight());
				GL11.glVertex2f(x + texture.getImageWidth(), y + texture.getImageHeight());
				GL11.glTexCoord2f(0, texture.getHeight());
				GL11.glVertex2f(x, y + texture.getImageHeight());
				GL11.glEnd();
			}catch (err){
				// Utils.log(x.toString()+", "+y.toString(), this);
				// Utils.log(err, this);
			}
		}else{
			Utils.log("X or Y is NaN", this);
		}
	}

	this.renderTexture = function(texture, x, y) {
		x = parseInt(x);
		y = parseInt(y);
		if (typeof(x) === "number" && typeof(y) === "number"){
			try{
				texture.bind();
				GL11.glColor3f(1, 1, 1);
				GL11.glBegin(GL11.GL_QUADS);
				GL11.glTexCoord2f(0, 0);
				GL11.glVertex2f(x, y);
				GL11.glTexCoord2f(texture.getWidth(), 0);
				GL11.glVertex2f(x + texture.getImageWidth(), y);
				GL11.glTexCoord2f(texture.getWidth(), texture.getHeight());
				GL11.glVertex2f(x + texture.getImageWidth(), y + texture.getImageHeight());
				GL11.glTexCoord2f(0, texture.getHeight());
				GL11.glVertex2f(x, y + texture.getImageHeight());
				GL11.glEnd();
			}catch (err){
				// Utils.log(x.toString()+", "+y.toString(), this);
				// Utils.log(err, this);
			}
		}else{
			Utils.log("X or Y is NaN", this);
		}
	}

	this.loadFont = function(name, size){
		if (new File("./assets/"+name+".ttf").exists()) {
			try{
				var inputStream = ResourceLoader.getResourceAsStream("./assets/"+name+".ttf");
				var awtFont2 = java.awt.Font.createFont(java.awt.Font.TRUETYPE_FONT, inputStream);
				awtFont2 = awtFont2.deriveFont(size); // set font size
				return TrueTypeFont(awtFont2, false);
			}catch (e){
				Utils.log(e, this);
			}
		} else {
			Utils.log("Font "+name+" could not be loaded", null);
		}
	}

	this.FPS = new FPS();
	
}

function FPS(){
	this.lastFrameTime = 0;
	this.currentFrameTime = 0;

	this.frameTime = function(){
		var d = this.getTime();
		this.currentFrameTime = d - this.lastFrameTime;
		this.lastFrameTime = d;
	}

	this.lastFrame = 0;
	this.fps = 0;
	this.lastFPS = 0;
	this.actualFPS = 0;

	this.new = function() {
		this.getDelta();
		this.lastFPS = this.getTime();
	}
	
	this.getDelta = function() {
		var time = this.getTime();
		var delta = (time - this.lastFrame);
		this.lastFrame = time;
		return delta;
	}

	this.getTime = function() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}
	
	this.getFPS = function(){
		return this.actualFPS;
	}

	this.update = function() {
		this.getDelta();
		if (this.getTime() - this.lastFPS > 1000) {
			this.actualFPS = this.fps;
			this.fps = 0;
			this.lastFPS += 1000;
		}
		this.fps++;
	}

	this.new();
}
//ModelLoader.js
/**
 * @author BeryJu, ported from CodingUniverse
 */
importPackage(org.lwjgl);
importPackage(org.lwjgl.util.vector);
importPackage(java.io);
importPackage(java.nio);
importPackage(org.lwjgl.opengl.GL11);
importPackage(org.lwjgl.opengl.GL15);
function ModelLoader(){
	this.createDisplayList = function(m) {
		var displayList = glGenLists(1);
		glNewList(displayList, GL_COMPILE);
		glMaterialf(GL_FRONT, GL_SHININESS, 128);
		glBegin(GL_TRIANGLES);
		foreach(m.faces, function(e){
			var n1 = m.normals.get(parseInt(e.normal.x - 1));
			glNormal3f(n1.x, n1.y, n1.z);
			var v1 = m.vertices.get(parseInt(e.vertex.x - 1));
			glVertex3f(v1.x, v1.y, v1.z);
			var n2 = m.normals.get(parseInt(e.normal.y - 1));
			glNormal3f(n2.x, n2.y, n2.z);
			var v2 = m.vertices.get(parseInt(e.vertex.y - 1));
			glVertex3f(v2.x, v2.y, v2.z);
			var n3 = m.normals.get(parseInt(e.normal.z - 1));
			glNormal3f(n3.x, n3.y, n3.z);
			var v3 = m.vertices.get(parseInt(e.vertex.z - 1));
			glVertex3f(v3.x, v3.y, v3.z);
		})
		glEnd();
		glEndList();
		return displayList;
	}

	this.reserveData = function(size) {
		return BufferUtils.createFloatBuffer(size);
	}

	this.asFloats = function(v) {
		var arr = [v.x, v.y, v.z];
		return arr;
	}

	this.createVBO = function(model) {
		var vboVertexHandle = glGenBuffers();
		var vboNormalHandle = glGenBuffers();
		var vertices = reserveData(model.faces.size() * 9);
		var normals = reserveData(model.faces.size() * 9);
		foreach(model.faces, function(e){
			vertices.put(asFloats(model.vertices.get(parseInt(face.vertex.x - 1))));
			vertices.put(asFloats(model.vertices.get(parseInt(face.vertex.y - 1))));
			vertices.put(asFloats(model.vertices.get(parseInt(face.vertex.z - 1))));
			normals.put(asFloats(model.normals.get(parseInt(face.normal.x - 1))));
			normals.put(asFloats(model.normals.get(parseInt(face.normal.y - 1))));
			normals.put(asFloats(model.normals.get(parseInt(face.normal.z - 1))));
		});
		vertices.flip();
		normals.flip();
		glBindBuffer(GL_ARRAY_BUFFER, vboVertexHandle);
		glBufferData(GL_ARRAY_BUFFER, vertices, GL_STATIC_DRAW);
		glVertexPointer(3, GL_FLOAT, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, vboNormalHandle);
		glBufferData(GL_ARRAY_BUFFER, normals, GL_STATIC_DRAW);
		glNormalPointer(GL_FLOAT, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		return [vboVertexHandle, vboNormalHandle];
	}

	this.loadOBJ = function(f){
		var reader = new BufferedReader(new FileReader(f));
		var m = new Model();
		var line;
		while ((line = reader.readLine()) != null) {
			if (line.startsWith("v ")) {
				var x = parseFloat(line.split(" ")[1]);
				var y = parseFloat(line.split(" ")[2]);
				var z = parseFloat(line.split(" ")[3]);
				m.vertices.add(new Vector3f(x, y, z));
			} else if (line.startsWith("vn ")) {
				var x = parseFloat(line.split(" ")[1]);
				var y = parseFloat(line.split(" ")[2]);
				var z = parseFloat(line.split(" ")[3]);
				m.normals.add(new Vector3f(x, y, z));
			} else if (line.startsWith("vt ")) {

			} else if (line.startsWith("f ")) {
				var vertexIndices = new Vector3f(parseFloat(line.split(" ")[1].split("/")[0]),
						parseFloat(line.split(" ")[2].split("/")[0]),
						parseFloat(line.split(" ")[3].split("/")[0]));
				var normalIndices = new Vector3f(parseFloat(line.split(" ")[1].split("/")[2]),
						parseFloat(line.split(" ")[2].split("/")[2]),
						parseFloat(line.split(" ")[3].split("/")[2]));
				m.faces.add(new Face(vertexIndices, normalIndices));
			}
		}
		reader.close();
		return m;
	}
}
//Input.js
importPackage(org.lwjgl.Input.Keyboard);
importPackage(org.lwjgl.Input.Mouse);

function InputAction(key, mode, callback){
	//Private inner class
	this.key = key;

	this.mode = mode;
	this.action = callback;
}

function Input(){
	
	this.Mouse = function(){
		return org.lwjgl.input.Mouse;
	}

	this.getMousePos = function(){
		var vec = new Vector2(0, 0);
		vec.setX(org.lwjgl.input.Mouse.getX());
		vec.setY(new Display().getHeight() - org.lwjgl.input.Mouse.getY());
		return vec;
	}

	this.InputActions = [];

	this.Keyboard = function(){
		return org.lwjgl.input.Keyboard;
	}

	this.bind = function(key, mode, action){
		Utils.log("Added binding to Input stack", this);
		this.InputActions.push(new InputAction(key, mode, action));
	}

	this.KEY_MODE_TAP = 0;
	this.KEY_MODE_RELEASE = 1;
	this.KEY_MODE_PRESS = 2;

	this.check = function(parent){
		foreach(this.InputActions, function(Action){
			if (org.lwjgl.input.Keyboard.isKeyDown(Action.key)) {
				if (Action.mode == 2){
					Action.action(parent);
				}
			}
		});
		while (org.lwjgl.input.Keyboard.next()) {
			if (org.lwjgl.input.Keyboard.getEventKeyState()) {
				foreach(this.InputActions, function(Action){
					if (Action.mode == 0){
						if (org.lwjgl.input.Keyboard.getEventKey() == Action.key){
							Action.action(parent);
						}
					}
				})
			}else{
				foreach(this.InputActions, function(Action){
					if (Action.mode == 1){
						if (org.lwjgl.input.Keyboard.getEventKey() == Action.key){
							Action.action(parent);
						}
					}
				})
			}
		}
	}

	//All the keycodes
	this.CHAR_NONE = 0;
	this.EVENT_SIZE = 18;
	this.KEY_0 = 11;
	this.KEY_1 = 2;
	this.KEY_2 = 3;
	this.KEY_3 = 4;
	this.KEY_4 = 5;
	this.KEY_5 = 6;
	this.KEY_6 = 7;
	this.KEY_7 = 8;
	this.KEY_8 = 9;
	this.KEY_9 = 10;
	this.KEY_A = 30;
	this.KEY_ADD = 78;
	this.KEY_APOSTROPHE = 40;
	this.KEY_APPS = 221;
	this.KEY_AT = 145;
	this.KEY_AX = 150;
	this.KEY_B = 48;
	this.KEY_BACK = 14;
	this.KEY_BACKSLASH = 43;
	this.KEY_C = 46;
	this.KEY_CAPITAL = 58;
	this.KEY_CIRCUMFLEX = 144;
	this.KEY_COLON = 146;
	this.KEY_COMMA = 51;
	this.KEY_CONVERT = 121;
	this.KEY_D = 32;
	this.KEY_DECIMAL = 83;
	this.KEY_DELETE = 211;
	this.KEY_DIVIDE = 181;
	this.KEY_DOWN = 208;
	this.KEY_E = 18;
	this.KEY_END = 207;
	this.KEY_EQUALS = 13;
	this.KEY_ESCAPE = 1;
	this.KEY_F = 33;
	this.KEY_F1 = 59;
	this.KEY_F10 = 68;
	this.KEY_F11 = 87;
	this.KEY_F12 = 88;
	this.KEY_F13 = 100;
	this.KEY_F14 = 101;
	this.KEY_F15 = 102;
	this.KEY_F2 = 60;
	this.KEY_F3 = 61;
	this.KEY_F4 = 62;
	this.KEY_F5 = 63;
	this.KEY_F6 = 64;
	this.KEY_F7 = 65;
	this.KEY_F8 = 66;
	this.KEY_F9 = 67;
	this.KEY_G = 34;
	this.KEY_GRAVE = 41;
	this.KEY_H = 35;
	this.KEY_HOME = 199;
	this.KEY_I = 23;
	this.KEY_INSERT = 210;
	this.KEY_J = 36;
	this.KEY_K = 37;
	this.KEY_KANA = 112;
	this.KEY_KANJI = 148;
	this.KEY_L = 38;
	this.KEY_LBRACKET = 26;
	this.KEY_LCONTROL = 29;
	this.KEY_LEFT = 203;
	this.KEY_LMENU = 56;
	this.KEY_LMETA = 219;
	this.KEY_LSHIFT = 42;
	this.KEY_LWIN = 219;
	this.KEY_M = 50;
	this.KEY_MINUS = 12;
	this.KEY_MULTIPLY = 55;
	this.KEY_N = 49;
	this.KEY_NEXT = 209;
	this.KEY_NOCONVERT = 123;
	this.KEY_NONE = 0;
	this.KEY_NUMLOCK = 69;
	this.KEY_NUMPAD0 = 82;
	this.KEY_NUMPAD1 = 79;
	this.KEY_NUMPAD2 = 80;
	this.KEY_NUMPAD3 = 81;
	this.KEY_NUMPAD4 = 75;
	this.KEY_NUMPAD5 = 76;
	this.KEY_NUMPAD6 = 77;
	this.KEY_NUMPAD7 = 71;
	this.KEY_NUMPAD8 = 72;
	this.KEY_NUMPAD9 = 73;
	this.KEY_NUMPADCOMMA = 179;
	this.KEY_NUMPADENTER = 156;
	this.KEY_NUMPADEQUALS = 141;
	this.KEY_O = 24;
	this.KEY_P = 25;
	this.KEY_PAUSE = 197;
	this.KEY_PERIOD = 52;
	this.KEY_POWER = 222;
	this.KEY_PRIOR = 201;
	this.KEY_Q = 16;
	this.KEY_R = 19;
	this.KEY_RBRACKET = 27;
	this.KEY_RCONTROL = 157;
	this.KEY_RETURN = 28;
	this.KEY_RIGHT = 205;
	this.KEY_RMENU = 184;
	this.KEY_RMETA = 220;
	this.KEY_RSHIFT = 54;
	this.KEY_RWIN = 220;
	this.KEY_S = 31;
	this.KEY_SCROLL = 70;
	this.KEY_SEMICOLON = 39;
	this.KEY_SLASH = 53;
	this.KEY_SLEEP = 223;
	this.KEY_SPACE = 57;
	this.KEY_STOP = 149;
	this.KEY_SUBTRACT = 74;
	this.KEY_SYSRQ = 183;
	this.KEY_T = 20;
	this.KEY_TAB = 15;
	this.KEY_U = 22;
	this.KEY_UNDERLINE = 147;
	this.KEY_UNLABELED = 151;
	this.KEY_UP = 200;
	this.KEY_V = 47;
	this.KEY_W = 17;
	this.KEY_X = 45;
	this.KEY_Y = 21;
	this.KEY_YEN = 125;
	this.KEY_Z = 44;
	this.KEYBOARD_SIZE = 25;

}
//Model.js
/**
 * @author BeryJu, ported from CodingUniverse
 */
importPackage(java.uitl);
importPackage(org.lwjgl.util.vector);
function Model(){

	this.vertices = new ArrayList();
	this.normals = new ArrayList();

	this.faces = new ArrayList();

}

function Face(vertex, normal){

	this.vertex = vertex;
	this.normal = normal;

}
//Vector2.js
function Vector2(X, Y){

	/**
	 * X
	 * @type {number}
	 */
	this.X = X;
	/**
	 * Y
	 * @type {number}
	 */
	this.Y = Y;

	/**
	 * Get X
	 * @return {number} X of this
	 */
	this.getX = function(){
		return this.X;
	}

	/**
	 * Get Y
	 * @return {number} Y of this
	 */
	this.getY = function(){
		return this.Y;
	}

	/**
	 * Set X
	 * @param {number} newX new value X
	 */
	this.setX = function(newX){
		this.X = newX;
	}

	/**
	 * Set Y
	 * @param {number} newY new value Y
	 */
	this.setY = function(newY){
		this.Y = newY;
	}


	this.toString = function(){
		return "X: "+this.X.toString()+", Y: "+this.Y.toString();
	}

	this.equals = function(a){
		var q = a.getX() == this.getX();
		var w = a.getY() == this.getY();
		return q && w;
	}

}
VectorLength = function(v){
	return Math.sqrt(v.X*v.X+v.Y*v.Y)
}

VectorNormalized = function(v){
	var l = VectorLength(v)
	return new Vector2(v.X/l, v.Y/l);
}

VectorSub = function(v1, v2){
	return new Vector2(v1.X-v2.X, v1.Y-v2.Y);
}

VectorAdd = function(v1, v2){
	return new Vector2(v1.X+v2.X, v1.Y+v2.Y);
}

VectorMul = function(v1, v2){
	return new Vector2(v1.X*v2.X, v1.Y*v2.Y);
}

VectorInterpolate = function(obj, dest, speed){
	var dir = VectorNormalized(VectorSub(dest, obj))
	var dir2 = VectorAdd(obj, VectorMul(dir, speed));
	return VectorRound(dir2);
}

VectorRound = function(v){
	return new Vector2(Math.round(v.getX()),Math.round(v.getY()));
}
//ArrayUtils.js
function foreach(array, callback){
	for (var i = 0; i < array.length; i++) {
		callback(array[i]);
	}	
}
//Tile.js
function Tile(size, texture){

	this.size = size;
	this.texture = texture;

	this.getTile = function(X, Y){
		throw new NotImplementedException();
	}

}
//Display.js
function Display(){

	this.setDisplayMode = function(w, h){
		try{
			org.lwjgl.opengl.Display.setDisplayMode(new org.lwjgl.opengl.DisplayMode(w, h));
		}catch (err){
			Utils.log(err, this);
		}
	}

	this.setFullscreen = function(fullscreen){
		try{
			if (fullscreen == true){
				org.lwjgl.opengl.Display.setFullscreen(true);
			}else{
				org.lwjgl.opengl.Display.setFullscreen(false);
			}
		}catch (err){
			Utils.log(err, this);
		}
	}

	this.getWidth = function(){
		try{
			return org.lwjgl.opengl.Display.getDisplayMode().getWidth();
		}catch (err){
			Utils.log(err, this);
		}
	}

	this.getHeight = function(){
		try{
			return org.lwjgl.opengl.Display.getDisplayMode().getHeight();
		}catch (err){
			Utils.log(err, this);
		}
	}

	this.setTitle = function(title){
		try{
			org.lwjgl.opengl.Display.setTitle(title);
		}catch (err){
			Utils.log(err, this);
		}
	}

	this.create = function(){
		try{
			org.lwjgl.opengl.Display.create();
		}catch (err){
			Utils.log(err, this);
		}
	}

	this.requestClose = function(){
		Utils.log("Requested Close", this);
		this.destroy();
		return java.lang.System.exit(0);
	}

	this.isCloseRequested = function(){
		if (org.lwjgl.opengl.Display.isCloseRequested() == true){
			new AL().destroy();
			this.requestClose();
		}
		return false;
	}

	this.destroy = function(){
		org.lwjgl.opengl.Display.destroy();
	}

}
//Hitbox.js
importClass(org.lwjgl.util.Rectangle);
function Hitbox(X, Y, Width, Height){

	this.X = X;
	this.Y = Y;
	this.Width = Width;
	this.Height = Height;

	this.intersects = function(hitbox){
		if (typeof this === "undefined" ||
			typeof hitbox === "undefined"){
			return false;
		}else{
			var a = new Rectangle(this.X, this.Y, this.Width, this.Height);
			var b = new Rectangle(hitbox.X, hitbox.Y,hitbox.Width, hitbox.Height);
			return a.intersects(b);
		}
	}

}
//AL.js
importPackage(org.newdawn.slick.openal)
importPackage(org.newdawn.slick.util);

function Audio(){

	this._Data;
	
	this.sound = function(gain, pitch, loop){
		this._Data.playAsSoundEffect(gain, pitch, loop);
	}

	this.music = function(gain, pitch, loop){
		this._Data = playAsMusic(gain, pitch, loop);
	}

}

function AL(){
	
	this.put = function(){
		SoundStore.get().poll(0);
	}

	this.load = function(filename){
		Utils.log("Loaded Sound \'"+filename+"\'", this);
		var a = new Audio();
		a._Data = AudioLoader.getAudio("OGG", ResourceLoader.
			getResourceAsStream("assets/"+filename+".ogg"));
		return a;
	}

	this.destroy = function(){
		org.lwjgl.openal.AL.destroy();
	}

}
//Level.js






	
